package TestOOP.Abstraction;

import AttackOnOOP.Inheritance.FullTime;
import AttackOnOOP.Inheritance.PartTime;
import org.junit.Test;

public class TestAbstraction {

    @Test
    public void testPartTime() {
        PartTime partTime = new PartTime("Jack", 22, 6);
        partTime.calculateSalary();
        partTime.calculateSalary(52);
        partTime.SalaryPerWeek(10);
    }

    @Test
    public void testFullTime() {
        FullTime fullTime = new FullTime("Bob", 70);
        fullTime.calculateSalary();
        fullTime.calculateSalary(22);
        fullTime.SalaryPerWeek(15);

    }
}
