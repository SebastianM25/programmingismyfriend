package TestOOP.Polymorphism;

import AttackOnOOP.Polymorphism.Overloading;
import AttackOnOOP.Polymorphism.Overriding;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestPolymorphism {
    @Test
    public void Overloading() {
        Overloading overloading = new Overloading();
        overloading.add(6, 9);
        assertEquals(15,overloading.add(6,9));
    }

    @Test
    public void Overriding() {
        Overriding overriding = new Overriding();

    }
}
