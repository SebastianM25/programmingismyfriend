package TestOOP.Inheritance;

import AttackOnOOP.Inheritance.Person;
import AttackOnOOP.Interface.Intervention;
import org.junit.Test;

public class Doctor extends Person implements Intervention {
    @Test
    public void testDoctor(){
        Person person=new Person();
        person.name("Ralph");
        person.function("Neurology");
        System.out.println("==========");
        Doctor doctor=new Doctor();
        doctor.Intervention();
        doctor.work();
    }

    public void Intervention() {
        System.out.println("The doctor is doing an operation now");
    }
    @Override
    public void work(){
        System.out.println("The doctor has night program");
    }
}
