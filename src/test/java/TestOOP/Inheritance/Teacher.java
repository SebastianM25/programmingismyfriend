package TestOOP.Inheritance;

import AttackOnOOP.Inheritance.Person;
import AttackOnOOP.Interface.Teach;
import org.junit.Test;

public class Teacher extends Person implements Teach {


    @Test
    public void testTeacher() {
        Person person = new Person();
        person.name("Jane");
        person.function("English");

        System.out.println("========");
        Teacher teacher =new Teacher();
        teacher.teach();
        teacher.work();
    }

    public void teach() {
        System.out.println("The teacher is teaching the lesson now.");
    }
    @Override
    public void work(){
        System.out.println("The teacher has morning program");
    }
}
