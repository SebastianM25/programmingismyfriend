package TestOOP.Inheritance;

import AttackOnOOP.Inheritance.Person;
import AttackOnOOP.Interface.Learn;
import org.junit.Test;

public class Student extends Person implements Learn {
    @Test
    public void testStudent(){
        Person person = new Person();
        person.name("John");
        person.function("Mathematician");
        System.out.println("========");
        Student student=new Student();
        student.learn();
        student.work();
    }

    public void learn() {
        System.out.println("The student is learning now");
    }
    @Override
    public void work(){
        System.out.println("The student has a flexible program");
    }
}
