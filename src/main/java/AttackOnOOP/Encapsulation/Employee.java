package AttackOnOOP.Encapsulation;

public class Employee {

    private String name;
    private int cnp;
    private int age;
    private double salary;

    public String getName() {
        return name;
    }

    public int getCnp() {
        return cnp;
    }

    public int getAge() {
        return age;
    }

    public double getSalary() {
        return salary;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCnp(int cnp) {
        this.cnp = cnp;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
