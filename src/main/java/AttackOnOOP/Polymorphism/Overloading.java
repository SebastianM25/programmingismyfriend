package AttackOnOOP.Polymorphism;

public class Overloading {

    public void add() {
        System.out.println("Nothing to do here");

    }

    public int add(int a, int b) {
        int sum = a + b;
        //System.out.println("The result is: " + sum);
        return sum;
    }

    public void add(double a, double b) {
        double sum = a + b;
        System.out.println("The result is:" + sum);
    }

    public void add(int a, int b, int c) {
        int sum = a + b + c;
        System.out.println("The result is:" + sum);
    }

    public void add(int a, int b, boolean printResult) {

    }

    public void add(boolean printResult, int a, int b) {

    }

}
