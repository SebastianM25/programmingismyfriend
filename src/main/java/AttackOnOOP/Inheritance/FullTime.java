package AttackOnOOP.Inheritance;

import AttackOnOOP.Abstraction.Employee;

public class FullTime extends Employee {

    public FullTime(String name, int paymentPerHour) {
        super(name, paymentPerHour);
    }

    @Override
    public void calculateSalary() {
        System.out.println("The result for 8 hours: " + getPaymentPerHour() * 8);
    }

    @Override
    public void calculateSalary(int a) {
        if (getPaymentPerHour() * 8 < 500) {
            a = (getPaymentPerHour() * 8) * 2;
            System.out.println("New salary is: " + a);
        } else {
            System.out.println("Remains the same salary.");
        }
    }

    @Override
    public void SalaryPerWeek(int a) {
        a = (getPaymentPerHour() * 8) * 7;
        System.out.println("Salary per week is: "+a);
    }
}
