package AttackOnOOP.Inheritance;

import AttackOnOOP.Abstraction.Employee;
import org.junit.Test;

public class PartTime extends Employee {

    private int workingHours;

    public PartTime(String name, int paymentPerHour, int workingHours) {
        super(name, paymentPerHour);
        this.workingHours = workingHours;
    }

    @Override
    public void calculateSalary() {
        System.out.println("The result for 6 hours: " + getPaymentPerHour() * workingHours);
    }

    @Override
    public void calculateSalary(int a) {
        a = (getPaymentPerHour() * workingHours) * 52;
        System.out.println("Annula salary: " + a);
    }

    @Override
    public void SalaryPerWeek(int a) {
        a = (getPaymentPerHour() * workingHours) * 7;
        System.out.println("The salary per week is: " + a);
    }

}
