package AttackOnOOP.Inheritance;

public class Person {
    String name;
    int age;
    String function;
    String gender;




    public void name(String name) {
        System.out.println("The name is: " + name);
    }

    public void function(String function) {
        System.out.println("The function is: " + function);
    }

    public void work() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}
